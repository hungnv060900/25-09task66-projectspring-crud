package com.devcamp.provincerestapi.responsitory;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincerestapi.models.Province;

public interface IProvinceResponsitory extends JpaRepository<Province,Integer> {
    
}
