package com.devcamp.provincerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinceRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinceRestapiApplication.class, args);
	}

}
