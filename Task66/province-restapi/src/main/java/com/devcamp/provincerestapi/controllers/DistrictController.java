package com.devcamp.provincerestapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincerestapi.models.District;
import com.devcamp.provincerestapi.models.Province;
import com.devcamp.provincerestapi.responsitory.IDistrictResponsitory;
import com.devcamp.provincerestapi.responsitory.IProvinceResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/district")
public class DistrictController {
    @Autowired
    IDistrictResponsitory pDistrictResponsitory;

    @Autowired
    IProvinceResponsitory provinceResponsitory;

    //Get all 
    @GetMapping("/all")
	public List<District> getAllDistrict() {
		return pDistrictResponsitory.findAll();
	}

    //Get a district
    @GetMapping("/detail/{id}")
	public District getDistrictById(@PathVariable int id) {
		if (pDistrictResponsitory.findById(id).isPresent())
			return pDistrictResponsitory.findById(id).get();
		else
			return null;
	}

    //Get by provinceId
    @GetMapping("/{provinceId}/districts")
    public List < District > getDistrictByProvince(@PathVariable(value = "provinceId") int provinceId) {
        return pDistrictResponsitory.findByProvinceId(provinceId);
    }

    //Create a new district
    @PostMapping("/create/{id}")
	public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District district) {
		try {
			Optional<Province> provinceData = provinceResponsitory.findById(id);
			if (provinceData.isPresent()) {
			District newRole = new District();
			newRole.setId(district.getId());
			newRole.setName(district.getName());
			newRole.setPrefix(district.getPrefix());
			newRole.setProvince(district.getProvince());
            newRole.setWards(district.getWards());

			// Province _province = provinceData.get();
			// newRole.setProvince(_province);
			
			District savedRole = pDistrictResponsitory.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified District: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
    //Update a district
    @PutMapping("update/{id}")
	public ResponseEntity<Object> updateDistrict(@PathVariable("id") int id, @RequestBody District district) {
		Optional<District> districtData = pDistrictResponsitory.findById(id);
		if (districtData.isPresent()) {
			District newDitrict = districtData.get();
			newDitrict.setName(district.getName());
			newDitrict.setPrefix(district.getPrefix());
			newDitrict.setProvince(district.getProvince());
            newDitrict.setWards(district.getWards());

			District savedDistrict = pDistrictResponsitory.save(newDitrict);
			return new ResponseEntity<>(savedDistrict, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    //delete a district
    @DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable int id) {
		try {
			pDistrictResponsitory.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
