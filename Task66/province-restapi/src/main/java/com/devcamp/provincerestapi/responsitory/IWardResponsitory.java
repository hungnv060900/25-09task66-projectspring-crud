package com.devcamp.provincerestapi.responsitory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincerestapi.models.Ward;

public interface IWardResponsitory extends JpaRepository<Ward,Integer> {
    List<Ward> findByDistrictId(int id);
}
