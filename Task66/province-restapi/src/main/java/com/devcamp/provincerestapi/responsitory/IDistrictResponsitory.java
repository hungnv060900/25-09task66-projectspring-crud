package com.devcamp.provincerestapi.responsitory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincerestapi.models.District;

public interface IDistrictResponsitory extends JpaRepository<District,Integer>{
    List<District> findByProvinceId(int id);
}
