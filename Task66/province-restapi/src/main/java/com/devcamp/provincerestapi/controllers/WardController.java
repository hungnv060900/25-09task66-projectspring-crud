package com.devcamp.provincerestapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincerestapi.models.District;
import com.devcamp.provincerestapi.models.Ward;
import com.devcamp.provincerestapi.responsitory.IDistrictResponsitory;
import com.devcamp.provincerestapi.responsitory.IWardResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/ward")
public class WardController {
    @Autowired
    IWardResponsitory pWardResponsitory;
    @Autowired
    IDistrictResponsitory pDistrictResponsitory;

    // Get all
    @GetMapping("/all")
    public List<Ward> getAllWard() {
        return pWardResponsitory.findAll();
    }

    // Get one ward
    @GetMapping("/detail/{id}")
    public Ward getRegionById(@PathVariable int id) {
        if (pWardResponsitory.findById(id).isPresent())
            return pWardResponsitory.findById(id).get();
        else
            return null;
    }

    // Get by id of district
    @GetMapping("/{districtId}/wards")
    public List<Ward> getWardByDistrict(@PathVariable(value = "districtId") int districtId) {
        return pWardResponsitory.findByDistrictId(districtId);
    }
    // Create

    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createWard(@PathVariable("id") int id, @RequestBody Ward ward) {
        try {
            Optional<District> districtData = pDistrictResponsitory.findById(id);
            if (districtData.isPresent()) {
                Ward newRole = new Ward();
                newRole.setId(ward.getId());
                newRole.setName(ward.getName());
                newRole.setPrefix(ward.getPrefix());
                newRole.setDistrict(ward.getDistrict());

                Ward savedRole = pWardResponsitory.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Ward: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    //Update
    @PutMapping("/update/{id}")
	public ResponseEntity<Object> updateWard(@PathVariable("id") int id, @RequestBody Ward ward) {
		Optional<Ward> wardData = pWardResponsitory.findById(id);
		if (wardData.isPresent()) {
			Ward newWard = wardData.get();
			newWard.setName(ward.getName());
            newWard.setPrefix(ward.getPrefix());
            newWard.setDistrict(ward.getDistrict());
			
			Ward saveWard = pWardResponsitory.save(newWard);
            return new ResponseEntity<>(saveWard, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
    //Delete
    @DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable int id) {
		try {
			pWardResponsitory.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
