package com.devcamp.provincerestapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincerestapi.models.Province;
import com.devcamp.provincerestapi.responsitory.IProvinceResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/province")
public class ProvinceController {
    @Autowired
    IProvinceResponsitory provinceResponsitory;

    // Get all
    @GetMapping("/all")
    public List<Province> getAllCountry() {
        return provinceResponsitory.findAll();
    }

    // Get one
    @GetMapping("/detail/{id}")
    public Province getProvinceById(@PathVariable int id) {
        if (provinceResponsitory.findById(id).isPresent())
            return provinceResponsitory.findById(id).get();
        else
            return null;
    }

    // Create new Province
    @PostMapping("/create")
    public ResponseEntity<Object> createProvince(@RequestBody Province province) {
        try {

            Province savedRole = provinceResponsitory.save(province);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Province: " + e.getCause().getCause().getMessage());
        }
    }

    // Update a privince
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProvince(@PathVariable("id") int id, @RequestBody Province province) {
        Optional<Province> provinceData = provinceResponsitory.findById(id);
        if (provinceData.isPresent()) {
            Province newProvince = provinceData.get();
            // newProvince.setId(province.getId());
            newProvince.setCode(province.getCode());
            newProvince.setName(province.getName());
            newProvince.setDistricts(province.getDistricts());
            Province saveProvince = provinceResponsitory.save(newProvince);
            return new ResponseEntity<>(saveProvince, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Delete a province
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteProvinceById(@PathVariable int id) {
        try {
            Optional<Province> optional = provinceResponsitory.findById(id);
            if (optional.isPresent()) {
                provinceResponsitory.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
